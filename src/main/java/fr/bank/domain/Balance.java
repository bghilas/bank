package fr.bank.domain;

import fr.bank.domain.exception.InsufficientBalanceAccountException;

import java.math.BigDecimal;

public record Balance(BigDecimal value) {

    public Balance add(Amount amount) {
        return new Balance(this.value.add(amount.value()));
    }

    public Balance subtract(Amount amount) {
        if(this.value.compareTo(amount.value()) < 0 )
            throw new InsufficientBalanceAccountException("you don't have enough money in your account to withdraw");
        return new Balance(this.value.subtract(amount.value()));
    }

}
