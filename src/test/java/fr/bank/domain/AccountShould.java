package fr.bank.domain;

import fr.bank.domain.exception.InsufficientBalanceAccountException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AccountShould {

    @Test
    public void create_account_with_balance_zero() {

        Account account = new Account(new Balance(BigDecimal.ZERO), LocalDateTime.now());

        Assertions.assertEquals(new Balance(BigDecimal.ZERO), account.getBalance());

    }

    @Test
    public void add_20_from_initial_balance_of_0_when_deposit_20() {

        Account account = new Account(new Balance(BigDecimal.ZERO), LocalDateTime.now());

        account.deposit(new Amount(new BigDecimal("20")), LocalDateTime.now());

        Assertions.assertEquals(new Balance(new BigDecimal("20")), account.getBalance());

    }

    @Test
    public void subtract_10_from_initial_balance_of_20_when_withdraw_10() {

        Account account = new Account(new Balance(new BigDecimal("20")), LocalDateTime.now());

        account.withdraw(new Amount(BigDecimal.TEN), LocalDateTime.now());

        Assertions.assertEquals(new Balance(BigDecimal.TEN), account.getBalance());

    }

    @Test
    public void forbidden_withdraw_when_insufficient_balance() {

        Account account = new Account(new Balance(BigDecimal.TEN), LocalDateTime.now());

        Assertions.assertThrows(InsufficientBalanceAccountException.class, () -> account.withdraw(new Amount(new BigDecimal("30")), LocalDateTime.now()));
    }

    @Test
    public void trace_create_account_operation() {

        LocalDateTime dateTimeCreationAccount = LocalDateTime.now();
        Operation operationExpected = new Operation(OperationType.CREATE, new Amount(BigDecimal.TEN), dateTimeCreationAccount);
        AccountHistoryLine accountHistoryLineExpected = new AccountHistoryLine(operationExpected, new Balance(BigDecimal.TEN));

        Account account = new Account(new Balance(BigDecimal.TEN), dateTimeCreationAccount);

        Assertions.assertEquals(accountHistoryLineExpected, account.getOperationHistory().getList().get(0));

    }

    @Test
    public void trace_deposit_operation() {

        Account account = new Account(new Balance(BigDecimal.ZERO), LocalDateTime.now());
        LocalDateTime localDateTimeDeposit = LocalDateTime.now();
        Operation operationExpected = new Operation(OperationType.DEPOSIT, new Amount(BigDecimal.TEN), localDateTimeDeposit);
        AccountHistoryLine accountHistoryLineExpected = new AccountHistoryLine(operationExpected, new Balance(BigDecimal.TEN));


        account.deposit(new Amount(BigDecimal.TEN), localDateTimeDeposit);

        Assertions.assertEquals(accountHistoryLineExpected, account.getOperationHistory().getList().get(1));
    }


    @Test
    public void trace_withdrawal_operation() {

        Account account = new Account(new Balance(BigDecimal.TEN), LocalDateTime.now());
        LocalDateTime localDateTimeWithdrawal = LocalDateTime.now();
        Operation operationExpected = new Operation(OperationType.WITHDRAWAL, new Amount(BigDecimal.TEN), localDateTimeWithdrawal);
        AccountHistoryLine accountHistoryLineExpected = new AccountHistoryLine(operationExpected, new Balance(BigDecimal.ZERO));


        account.withdraw(new Amount(BigDecimal.TEN), localDateTimeWithdrawal);

        Assertions.assertEquals(accountHistoryLineExpected, account.getOperationHistory().getList().get(1));
    }

    @Test
    public void trace_create_account_and_withdrawal_and_deposit() {
        //Given
        LocalDateTime localDateTimeCreationAccount = LocalDateTime.now();
        LocalDateTime localDateTimeWithdraw = LocalDateTime.now();
        LocalDateTime localDateTimeDeposit = LocalDateTime.now();
        AccountHistory accountHistoryLineExpected = new AccountHistory();
        accountHistoryLineExpected.addLine(OperationType.CREATE, new Amount(BigDecimal.TEN), localDateTimeCreationAccount, new Balance(BigDecimal.TEN));
        accountHistoryLineExpected.addLine(OperationType.WITHDRAWAL, new Amount(BigDecimal.TEN), localDateTimeWithdraw, new Balance(BigDecimal.ZERO));
        accountHistoryLineExpected.addLine(OperationType.DEPOSIT, new Amount(BigDecimal.TEN), localDateTimeDeposit, new Balance(BigDecimal.TEN));

        //when
        Account account = new Account(new Balance(BigDecimal.TEN), localDateTimeCreationAccount);
        account.withdraw(new Amount(BigDecimal.TEN), localDateTimeWithdraw);
        account.deposit(new Amount(BigDecimal.TEN), localDateTimeDeposit);

        //then
        Assertions.assertEquals(accountHistoryLineExpected.getList(), account.getOperationHistory().getList());

    }

    @Test
    public void print_account_history() {
        //Given
        LocalDateTime dateTimeCreationAccount = LocalDateTime.now();
        LocalDateTime dateTimeDeposit = LocalDateTime.now();
        List<AccountHistoryLine> accountHistoryLineExpected = new ArrayList<>();

        Account account = new Account(new Balance(BigDecimal.ZERO), dateTimeCreationAccount);
        account.deposit(new Amount(BigDecimal.TEN), dateTimeDeposit);

        accountHistoryLineExpected.add(new AccountHistoryLine(
                new Operation(OperationType.CREATE, new Amount(BigDecimal.ZERO), dateTimeCreationAccount),
                new Balance(BigDecimal.ZERO)));
        accountHistoryLineExpected.add(new AccountHistoryLine(
                new Operation(OperationType.DEPOSIT, new Amount(BigDecimal.TEN), dateTimeDeposit),
                new Balance(BigDecimal.TEN)));

        //When
        FakeAccountHistoryPrinter printer = new FakeAccountHistoryPrinter();
        account.printAccountHistory(printer);

        //Then
        Assertions.assertEquals(accountHistoryLineExpected, printer.getLines());

    }

}

