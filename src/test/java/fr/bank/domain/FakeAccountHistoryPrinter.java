package fr.bank.domain;

import java.util.List;

public class FakeAccountHistoryPrinter implements AccountHistoryPrinter {

    private List<AccountHistoryLine> lines;

    @Override
    public void print(AccountHistory accountHistory) {
        lines = accountHistory.getList();
    }

    public List<AccountHistoryLine> getLines() {
        return lines;
    }
}
