package fr.bank.domain;

public enum OperationType {
    CREATE, DEPOSIT, WITHDRAWAL;
}
