package fr.bank.domain;

import java.time.LocalDateTime;

public class Account {

    private Balance balance;
    private AccountHistory accountHistory = new AccountHistory();

    public Account(Balance balance, LocalDateTime localDateTime) {
        this.balance = balance;
        accountHistory.addLine(OperationType.CREATE, new Amount(balance.value()), localDateTime, balance);
    }

    public void deposit(Amount amount, LocalDateTime localDateTime) {
        balance = balance.add(amount);
        accountHistory.addLine(OperationType.DEPOSIT, amount, localDateTime, balance);
    }

    public void withdraw(Amount amount, LocalDateTime localDateTime) {
        balance = balance.subtract(amount);
        accountHistory.addLine(OperationType.WITHDRAWAL, amount, localDateTime, balance);
    }

    public Balance getBalance() {
        return balance;
    }

    public AccountHistory getOperationHistory() {
        return accountHistory;
    }

    public void printAccountHistory(AccountHistoryPrinter accountHistoryPrinter) {
        accountHistoryPrinter.print(accountHistory);
    }
}
