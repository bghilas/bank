package fr.bank.infrastructure;

import fr.bank.domain.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class ConsoleAccountHistoryPrinterShould {

    AccountHistory accountHistoryToPrint = new AccountHistory();
    StringBuilder expectedPrint = new StringBuilder(ConsoleAccountHistoryPrinter.HEADER);

    @Test
    public void print_history_account() {


        add_operation_to_print(OperationType.CREATE, new Amount(BigDecimal.ZERO), LocalDateTime.now(), new Balance(BigDecimal.ZERO));
        add_operation_to_print(OperationType.DEPOSIT, new Amount(BigDecimal.TEN), LocalDateTime.now(), new Balance(BigDecimal.TEN));
        add_operation_to_print(OperationType.WITHDRAWAL, new Amount(BigDecimal.TEN), LocalDateTime.now(), new Balance(BigDecimal.ZERO));

        ConsoleAccountHistoryPrinter consoleHistoryAccountPrinter = new ConsoleAccountHistoryPrinter();
        consoleHistoryAccountPrinter.print(accountHistoryToPrint);

        Assertions.assertEquals(expectedPrint.toString(), consoleHistoryAccountPrinter.getOut());

    }


    private void add_operation_to_print(OperationType operationType, Amount amount, LocalDateTime localDateTime, Balance balance) {

        accountHistoryToPrint.getList().add(
                new AccountHistoryLine(
                        new Operation(operationType, amount, localDateTime),
                        balance
                ));
        expectedPrint.append(operationType)
                .append(ConsoleAccountHistoryPrinter.SEPARATOR)
                .append(amount.value()).append(ConsoleAccountHistoryPrinter.CURRENCY)
                .append(ConsoleAccountHistoryPrinter.SEPARATOR)
                .append(localDateTime.format(ConsoleAccountHistoryPrinter.formatter))
                .append(ConsoleAccountHistoryPrinter.SEPARATOR)
                .append(balance.value()).append(ConsoleAccountHistoryPrinter.CURRENCY)
                .append(ConsoleAccountHistoryPrinter.NEW_LINE);
    }


}
