package fr.bank.domain;

public interface AccountHistoryPrinter {

    void print(AccountHistory accountHistory);

}
