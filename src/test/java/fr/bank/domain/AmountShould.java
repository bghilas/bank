package fr.bank.domain;

import fr.bank.domain.exception.NegativeAmountException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class AmountShould {


    @Test
    public void amount_be_positive(){

        Account account = new Account(new Balance(BigDecimal.ZERO), LocalDateTime.now());

        Assertions.assertThrows(NegativeAmountException.class, () -> account.deposit(new Amount(new BigDecimal("-10")), LocalDateTime.now()));

    }

}

