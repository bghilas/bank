package fr.bank.domain;

import fr.bank.domain.exception.NegativeAmountException;

import java.math.BigDecimal;

public record Amount (BigDecimal value){

    public Amount {
        if(value.compareTo(new BigDecimal("0")) == -1 )
            throw new NegativeAmountException("The amount should be positive");
    }
}
