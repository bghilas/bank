package fr.bank.infrastructure;

import fr.bank.domain.AccountHistory;
import fr.bank.domain.AccountHistoryPrinter;

import java.time.format.DateTimeFormatter;

public class ConsoleAccountHistoryPrinter implements AccountHistoryPrinter {

    private String out;
    public static final String SEPARATOR = " | ";
    public static final String NEW_LINE = "\n";
    public static final String HEADER = "Operation | Amount | Date | Balance \n ";
    public static final String CURRENCY = "€ ";
    public static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    @Override
    public void print(AccountHistory accountHistory) {

        StringBuilder out = new StringBuilder(HEADER);

        accountHistory.getList().forEach(line -> {
            out.append(line.operation().operationType()).append(SEPARATOR)
                    .append(line.operation().amount().value()).append(CURRENCY).append(SEPARATOR)
                    .append(line.operation().localDateTime().format(formatter)).append(SEPARATOR)
                    .append(line.balance().value()).append(CURRENCY)
                    .append(NEW_LINE);
        });
        this.out = out.toString();

        System.out.println(out);

    }

    public String getOut() {
        return out;
    }
}
