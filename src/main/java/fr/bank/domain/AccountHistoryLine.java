package fr.bank.domain;

public record AccountHistoryLine(Operation operation, Balance balance){

}
