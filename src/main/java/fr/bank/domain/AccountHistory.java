package fr.bank.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AccountHistory {

    private  List<AccountHistoryLine> list = new ArrayList<>();

    public List<AccountHistoryLine> getList() {
        return list;
    }

    public void addLine(OperationType operationType, Amount amount, LocalDateTime localDateTime, Balance balance){
        Operation operationCreation = new Operation(operationType, amount, localDateTime);
        AccountHistoryLine accountHistoryLine = new AccountHistoryLine(operationCreation, balance);
        this.list.add(accountHistoryLine);
    }

}
