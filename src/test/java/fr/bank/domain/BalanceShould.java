package fr.bank.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class BalanceShould {

    @Test
    public void add_10_to_balance(){

        Balance balance = new Balance(BigDecimal.ZERO);

        balance = balance.add(new Amount(BigDecimal.TEN));

        Assertions.assertEquals(new Balance(BigDecimal.TEN), balance);

    }

    @Test
    public void subtract_10_from_balance(){

        Balance balance = new Balance(BigDecimal.TEN);

        balance = balance.subtract(new Amount(BigDecimal.TEN));

        Assertions.assertEquals(new Balance(BigDecimal.ZERO), balance);

    }
}
