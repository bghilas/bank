package fr.bank.domain.exception;

public class InsufficientBalanceAccountException extends RuntimeException{

    public InsufficientBalanceAccountException(String message) {
        super(message);
    }
}
