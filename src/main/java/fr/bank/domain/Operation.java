package fr.bank.domain;

import java.time.LocalDateTime;

public record Operation(OperationType operationType, Amount amount, LocalDateTime localDateTime) {
}
